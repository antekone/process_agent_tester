#include <QCoreApplication>
#include <QTimer>
#include "QtEventPort.h"

QtEventPort::~QtEventPort() { }

bool QtEventPort::wait() {
    // If events are already pending, they will be processed and we return immediately thereafter
    // Otherwise, we block until new events arrive
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
    return false;
}

bool QtEventPort::poll() {
    // Process any pending events, but don't block
    QCoreApplication::processEvents();
    return false;
}

void QtEventPort::setRunnable(bool runnable) {
    isRunnable = runnable;

    if(runnable)
        // As per Qt docs, this will schedule run() to be called when all currently pending events have been processed.
        QTimer::singleShot(0, this, SLOT(run()));
}

void QtEventPort::run() {
    if (kjLoop)
        kjLoop->run();

    if (isRunnable)
        // Still runnable? OK, but wait your turn
        QTimer::singleShot(0, this, SLOT(run()));
}
