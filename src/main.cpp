#include <cstdio>
#include <iostream>
#include <memory>
#include <capnp/rpc-twoparty.h>
#include <kj/async-io.h>
#include "packets.capnp.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QEventLoop>
#include <QtCore/QTimer>
#include <QtNetwork/QTcpSocket>

#include "QtEventPort.h"
#include "QSocketWrapper.hpp"

#define LOG(...) { std::cout << "log: " << __VA_ARGS__ << "\n"; }

using std::make_shared;
using std::shared_ptr;

class Backend {
public:
    Backend(ProcessAgentRpc::Client c): c(c) { 
        LOG("got backend");

        auto p = c.bannerRequest().send().then([] (capnp::Response<ProcessAgentRpc::BannerResults> results) {
            LOG("got banner response");
        });
    }
private:
    ProcessAgentRpc::Client c;
};

class RPC {
public:
    RPC(): stream(socket) {
        QObject::connect(&socket, &QTcpSocket::connected, [this] () {
            onConnected();
        });
    }

    void connectToBackend() {
        socket.connectToHost("127.0.0.1", 56012, QAbstractSocket::ReadWrite, QAbstractSocket::IPv4Protocol);
    }

    void onConnected() {
        LOG("connected");
        client = make_shared<capnp::TwoPartyClient>(stream);
        backend = make_shared<Backend>(client->bootstrap().castAs<ProcessAgentRpc>());
    }

private:
    QTcpSocket socket;
    QSocketWrapper stream;
    shared_ptr<capnp::TwoPartyClient> client;
    shared_ptr<Backend> backend;
};

void doMain(QCoreApplication& app) {
    using namespace kj;
    using namespace capnp;

    AsyncIoContext ioContext = setupAsyncIo();
    Own<AsyncIoStream> ioStream;

    auto& network = ioContext.provider->getNetwork();
    auto connector = network.parseAddress("127.0.0.1", 56013).then([&ioContext] (Own<NetworkAddress> addr) {
        LOG("got network address");
        return addr->connect();
    });
    
    auto promise2 = connector.then([&ioContext, &ioStream] (Own<AsyncIoStream> stream) {
        return Promise<Own<AsyncIoStream>>(std::move(stream));
    });
    
    auto promise3 = promise2.then([] (Own<AsyncIoStream> stream) {
        LOG("ok");
        return stream;
    });

    auto exc = promise3.then([] (Own<AsyncIoStream> stream) { }, [] (kj::Exception&& e) {
        LOG("error: " << e.getDescription().cStr());
    });

    exc.wait(ioContext.waitScope);
    return;
}

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);

    //QtEventPort qtEventPort;
    //kj::EventLoop loop(qtEventPort);
    //qtEventPort.setLoop(&loop);
    //kj::WaitScope wsc(loop);

    //RPC rpc;
    doMain(app);
}
