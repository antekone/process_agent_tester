#pragma once
#include <QObject>
#include <kj/async.h>

class QtEventPort : public QObject, public kj::EventPort {
    // Simple EventPort implementation to allow a KJ event loop to run in a thread scheduled by a Qt event loop
    // Make sure to call setLoop with a pointer to the KJ event loop which will be sharing this thread as soon as
    // possible after construction.
    Q_OBJECT

public:
    virtual ~QtEventPort();

    void setLoop(kj::EventLoop* kjLoop) {
        // Store a pointer to the KJ event loop to call run() on when it needs to process events. The QtEventPort does
        // not take ownership of kjLoop. Make sure to call setLoop(nullptr) or destroy the QtEvenPort prior to
        // deallocating kjLoop.
        this->kjLoop = kjLoop;
    }

    // EventPort API
    virtual bool wait();
    virtual bool poll();
    virtual void setRunnable(bool runnable);

private:
    bool isRunnable = false;
    kj::EventLoop* kjLoop = nullptr;

private slots:
    void run();
};
